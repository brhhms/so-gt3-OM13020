.data

num1: .long 0
num2: .long 0
salida: .ascii "el resultado es: %d\n"
tipo: .ascii "%d"
msj1: .ascii "ingrese el primer valor: \0"
msj2: .ascii "ingrese el segundo valor: \0"
.data
.global main
main:
	pushl $msj1
	call printf
	addl $4, %esp
	pushl $num1
	pushl $tipo
	call scanf
	addl $4, %esp
	
	pushl $msj2
	call printf
	addl $4, %esp
	pushl $num2
	pushl $tipo
	call scanf
	addl $8, %esp

	movl num1, %eax
	addl $4, %esp
	movl $0, %ebx
	movl $1, %eax
	int $0x80
